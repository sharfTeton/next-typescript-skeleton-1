import { Routes } from '@/config/routes';

export const siteSettings = {
  name: 'Tetonprivate',
  author: "Sharfuddin",
  description: '',
  logo: {
    url: '/logo.png',
    alt: 'Tetonprivate',
    href: '/',
    width: 128,
    height: 40,
  },
  defaultLanguage: 'en',
  authorizedLinks: [
    { href: Routes.profile, label: 'auth-menu-profile' },
  ],
  authorizedLinksMobile: [
    { href: Routes.profile, label: 'auth-menu-profile' },
  ],
  dashboardSidebarMenu: [
    {
      href: Routes.profile,
      label: 'profile-sidebar-profile',
    },
    {
      href: Routes.help,
      label: 'profile-sidebar-help',
    },
    {
      href: Routes.logout,
      label: 'profile-sidebar-logout',
    },
  ],
  footer: {
    copyright: {
      name: 'Teton',
      href: '/',
    },
    address: 'X/2,Y/3, Tropical Tower 5th floor Z/0, A/1 Bir Uttam Ave',
    email: 'company@yourdomain.com',
    phone: '0000-1111-2222-333',
    social:[
      {
        icon: 'FacebookIcon',
        url: 'https://www.facebook.com/xxx'
      },
      {
        icon: 'InstagramIcon',
        url: 'https://www.instagram.com/xxx'
      },
      {
        icon: 'YouTubeIcon',
        url: 'http://youtube.com/xxx'
      },
    ],
    menus: [
      {
        title: 'text-explore',
        links: [
          {
            name: 'text-about-us',
            href: '/',
          },
          {
            name: 'text-sitemap',
            href: '/',
          },
          {
            name: 'text-bookmarks',
            href: '/',
          },
          {
            name: 'text-sign-join',
            href: '/',
          },
        ],
      },
      {
        title: 'text-customer-service',
        links: [
          {
            name: 'text-faq-help',
            href: Routes.help,
          },
          {
            name: 'text-contact-us',
            href: Routes.contactUs,
          },
        ],
      },
      {
        title: 'text-our-information',
        links: [
          {
            name: 'text-sitemap',
            href: '/',
          },
        ],
      },
    ],
  },
};
